package mod;

import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;

public class Util 
{
	public static void sendChatMessageAsMod(String message)
	{
		EnumChatFormatting color = EnumChatFormatting.AQUA;//color
		
		ChatStyle style = new ChatStyle();
		style.setColor(color);
		
		ChatComponentText chatComponent = new ChatComponentText(message);
		chatComponent.setChatStyle(style);
		
		Minecraft.getMinecraft().thePlayer.addChatComponentMessage(chatComponent);
	}
	
	/**
	 * 
	 * @param world minecraft world object
	 * @param pos Position of the block in question
	 * @param particle Type of particle to be spawned
	 */
	public static void spawParticlesAroundBlock(World world, BlockPos pos, EnumParticleTypes particle)
	{
		Random rand = new Random();
		
		int numberOfParticles = rand.nextInt(8) + 10;
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		
		for(int i = 0; i < numberOfParticles; i++)
		{
			world.spawnParticle(particle, x + rand.nextDouble(), y + rand.nextDouble(), z + rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2.0D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2.0D, new int[0]);
		}
	}
}
