package mod.init;

import mod.Enfermedades.ColeraPotion;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EntityUpdateEventHandler
{
	@SubscribeEvent
	public void onEntityUpdate(LivingUpdateEvent event)
	{
		if(event.entityLiving.isPotionActive(ColeraPotion.my_potion))
		{
			if (event.entityLiving.worldObj.rand.nextInt(50) == 0) 
			{
				event.entityLiving.attackEntityFrom(DamageSource.generic, 0.5F);
			}
		}
	}
}
