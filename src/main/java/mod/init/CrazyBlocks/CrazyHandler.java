package mod.init.CrazyBlocks;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.text.DefaultEditorKit.CutAction;

import net.minecraft.block.Block;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class CrazyHandler extends Thread
{
	private static CrazyHandler handler = null;
	private static volatile long delay = 2000;//2s
	private static volatile boolean shouldRun = true;
	
	private Object monitor = new Object();
	
	private volatile World world = null;
	private volatile ArrayList<BlockPos> positions;
	private volatile ArrayList<IBlockState> states;
	private Random myRandom = null;
	private int currentIndex = 0;
	
	private CrazyHandler()
	{
		this.positions = new ArrayList<BlockPos>();
		this.states = new ArrayList<IBlockState>();
		this.myRandom = new Random();
		
		this.states.add(CrazyBlockRed.my_block.getDefaultState());
		this.states.add(CrazyBlock.my_block.getDefaultState());
	}
	
	public static CrazyHandler getCrazyHandler()
	{
		if(handler == null)
		{
			handler = new CrazyHandler();
		}
		
		return handler;
	}
	
	public void registerCrazyBlock(BlockPos pos, World worldIn)
	{
		try
		{
			synchronized(this.monitor)
			{
				//Block selected = worldIn.getBlockState(pos).getBlock();
				
				/*if(selected instanceof CrazyBlock)
				{*/
					if(this.positions.contains(pos))
					{
						System.out.println("Crazy is already");
						return;
					}
					else
						System.out.println("Crazy not registered yet... adding");
					
					if(this.positions.add(pos))
						System.out.println("Crazy block added!");
					else
						System.out.println("Crazy not added because reasons!");
					
					world = worldIn;
				//}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void deregisterBlock(BlockPos pos)
	{
		synchronized (this.monitor)
		{
			if(this.positions.contains(pos))
			{
				this.positions.remove(pos);
			}
		}
	}
	
	public void run()
	{
		while(shouldRun)
		{
			try 
			{
				Thread.sleep(delay);
				
				synchronized (this.monitor)
				{
					for(BlockPos pos : this.positions)
					{
						if(this.world == null)
						{
							System.out.println("Crazy world is null");
							break;
						}
						
						IBlockState selectedNewState = getNewBlockState();
						
						this.world.setBlockState(pos, selectedNewState);
					}
				}
				
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public IBlockState getNewBlockState()
	{
		int s = this.states.size();
		
		currentIndex++;
		
		currentIndex = currentIndex % s;
		
		return this.states.get(currentIndex);
	}
	
	public void finish()
	{
		//save & quit
	}

}
