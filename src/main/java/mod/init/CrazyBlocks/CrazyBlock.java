package mod.init.CrazyBlocks;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CrazyBlock extends Block
{
	public static CrazyBlock my_block;

	public CrazyBlock()
	{
		super(Material.glass);
	}
	
	public static void init()
	{
		my_block = (CrazyBlock)new CrazyBlock().setUnlocalizedName("CrazyBlock");
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	@Override
	public final IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        IBlockState output = super.onBlockPlaced(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer);
        
        CrazyHandler.getCrazyHandler().registerCrazyBlock(pos, worldIn);
        
        return output;
    }

	/**
     * Called when a player destroys this Block
     */
	@Override
    public final void onBlockDestroyedByPlayer(World worldIn, BlockPos pos, IBlockState state) 
    {
    	CrazyHandler.getCrazyHandler().deregisterBlock(pos);
    }
}
