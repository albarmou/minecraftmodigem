package mod.init;

import mod.Mod1;
import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Termociclator2 extends Block
{
	public static Termociclator2 my_block;
	
	public Termociclator2(Material materialIn) 
	{
		super(materialIn);
		// TODO Auto-generated constructor stub
	}
	
	public static void init()
	{
		my_block = (Termociclator2) new Termociclator2(Material.wood).setUnlocalizedName("termociclator2");
		my_block.setHardness(5F);
		my_block.setResistance(5F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	//Cuando boton derecho
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {		
		playerIn.openGui(Mod1.myMod, Reference.GUIID_Termociclator2, worldIn, pos.getX(), pos.getY(), pos.getZ());
		
		return true;
    }

}
