package mod.init;

import mod.Mod1;
import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class Termociclator4 extends Block
{
	public static Termociclator4 my_block;
	
	public Termociclator4()
	{
		super(Material.wood);
	}
	
	public static void init()
	{
		my_block = (Termociclator4) new Termociclator4().setUnlocalizedName("termociclator4");

		my_block.setHardness(3F);
		my_block.setResistance(3F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	//Cuando boton derecho
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {		
		playerIn.openGui(Mod1.myMod, Reference.GUIID_Termociclator4, worldIn, pos.getX(), pos.getY(), pos.getZ());
		
		return true;
    }
}
