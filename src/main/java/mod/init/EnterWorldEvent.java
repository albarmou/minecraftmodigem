package mod.init;

import mod.Enfermedades.ColeraPotion;
import mod.Enfermedades.ColeraPotionEffect;
import mod.init.CrazyBlocks.CrazyHandler;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

public class EnterWorldEvent
{
	@SubscribeEvent
	public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event)
	{
		if(!CrazyHandler.getCrazyHandler().isAlive())
		{
			CrazyHandler.getCrazyHandler().start();
			System.out.println("CrazyHandler started!");
		}
		
		//event.player.addPotionEffect(new ColeraPotionEffect());
	}
	
	@SubscribeEvent
	public void onPlayerLoggedOut(PlayerEvent.PlayerLoggedOutEvent event)
	{ 
		if(CrazyHandler.getCrazyHandler().isAlive())
		{
			CrazyHandler.getCrazyHandler().finish();
		}
	}
}
