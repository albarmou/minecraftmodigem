package mod.CraftingManagers.Termociclator3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import mod.Mod1;
import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.world.World;
import AlexIngenieriaBiologica.Elementos.*;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Termociclator3CraftingManager
{
    /** The static instance of this class */
    private static Termociclator3CraftingManager instance = new Termociclator3CraftingManager();
    /** A list of all the recipes added */
    private List recipes;
    private static final String __OBFID = "CL_00000090";

    /**
     * Returns the static instance of this class
     */
    public static Termociclator3CraftingManager getInstance()
    {
        /** The static instance of this class */
    	if(instance == null)
    	{
    		instance = new Termociclator3CraftingManager();
    	}
        return instance;
    }

    private Termociclator3CraftingManager()
    {
        recipes = new ArrayList();       

        this.addRecipe(new ItemStack(Element_35s_Epif_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_35s.my_block, 'Y', Element_EPIF.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_35s_Phy_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_35s.my_block, 'Y', Element_Phy.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_35s_Phy_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_35s.my_block, 'Y', Element_Gal4Kdronpa.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_35s_Gal4KDronpa_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_35s.my_block, 'Y', Element_Ndronpa.my_block, 'Z', Element_Tnos.my_block});
        
        this.addRecipe(new ItemStack(Element_Etr8_LexPif_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Etr8.my_block, 'Y', Element_LexApif.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_Etr8_BD1KDronpa_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Etr8.my_block, 'Y', Element_BD1Kdronpa.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_Etr8_RecombinasaA_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Etr8.my_block, 'Y', Element_Recombinase_A.my_block, 'Z', Element_Tnos.my_block});
        
        this.addRecipe(new ItemStack(Element_Gal4_LacPIF_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Gal4.my_block, 'Y', Element_LACIPIF.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_Gal4_BD2Kdronpa_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Gal4.my_block, 'Y', Element_BD2KDronpa.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_Gal4_RecombinasaB_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Gal4.my_block, 'Y', Element_Recombinase_B.my_block, 'Z', Element_Tnos.my_block});
        
        this.addRecipe(new ItemStack(Element_LexA_azul_tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_LexA.my_block, 'Y', Element_Blue.my_block, 'Z', Element_Tnos.my_block});
        
        this.addRecipe(new ItemStack(Element_BD1_Amarillo_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_LacI.my_block, 'Y', Element_Yellow.my_block, 'Z', Element_Tnos.my_block});
        
        this.addRecipe(new ItemStack(Element_LacI_Rojo_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_BD1.my_block, 'Y', Element_Red.my_block, 'Z', Element_Tnos.my_block});
        
        this.addRecipe(new ItemStack(Element_BD2_Verde_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_BD2.my_block, 'Y', Element_Green.my_block, 'Z', Element_Tnos.my_block});        
        
        this.addRecipe(new ItemStack(Element_35s_GFP_Tnos.my_block, 1), new Object[]{"XYZ",'X',Element_35s.my_block, 'Y', Element_GFP.my_block, 'Z', Element_Tnos.my_block});
        this.addRecipe(new ItemStack(Element_Pnos_GFP_Tnos.my_block, 1), new Object[]{"XYZ", 'X', Element_Pnos.my_block, 'Y', Element_GFP.my_block, 'Z', Element_Tnos.my_block});
        
    	Collections.sort(this.recipes, new Termociclator3RecipeSorter(this));
    }

    /**
     * Adds a shaped recipe to the games recipe list.
     */
    public Termociclator3ShapedRecipes addRecipe(ItemStack stack, Object ... recipeComponents)
    {
        String s = "";
        int i = 0;
        int j = 0;
        int k = 0;

        if (recipeComponents[i] instanceof String[])
        {
            String[] astring = (String[])((String[])recipeComponents[i++]);

            for (int l = 0; l < astring.length; ++l)
            {
                String s1 = astring[l];
                ++k;
                j = s1.length();
                s = s + s1;
            }
        }
        else
        {
            while (recipeComponents[i] instanceof String)
            {
                String s2 = (String)recipeComponents[i++];
                ++k;
                j = s2.length();
                s = s + s2;
            }
        }

        HashMap hashmap;

        for (hashmap = Maps.newHashMap(); i < recipeComponents.length; i += 2)
        {
            Character character = (Character)recipeComponents[i];
            ItemStack itemstack1 = null;

            if (recipeComponents[i + 1] instanceof Item)
            {
                itemstack1 = new ItemStack((Item)recipeComponents[i + 1]);
            }
            else if (recipeComponents[i + 1] instanceof Block)
            {
                itemstack1 = new ItemStack((Block)recipeComponents[i + 1], 1, 32767);
            }
            else if (recipeComponents[i + 1] instanceof ItemStack)
            {
                itemstack1 = (ItemStack)recipeComponents[i + 1];
            }

            hashmap.put(character, itemstack1);
        }

        ItemStack[] aitemstack = new ItemStack[j * k];

        for (int i1 = 0; i1 < j * k; ++i1)
        {
            char c0 = s.charAt(i1);

            if (hashmap.containsKey(Character.valueOf(c0)))
            {
                aitemstack[i1] = ((ItemStack)hashmap.get(Character.valueOf(c0))).copy();
            }
            else
            {
                aitemstack[i1] = null;
            }
        }

        Termociclator3ShapedRecipes shapedrecipes = new Termociclator3ShapedRecipes(j, k, aitemstack, stack);
        this.recipes.add(shapedrecipes);
        return shapedrecipes;
    }

    /**
     * Adds a shapeless crafting recipe to the the game.
     *  
     * @param recipeComponents An array of ItemStack's Item's and Block's that make up the recipe.
     */
    public void addTermociclator3ShapelessRecipe(ItemStack stack, Object ... recipeComponents)
    {
        ArrayList arraylist = Lists.newArrayList();
        Object[] aobject = recipeComponents;
        int i = recipeComponents.length;

        for (int j = 0; j < i; ++j)
        {
            Object object1 = aobject[j];

            if (object1 instanceof ItemStack)
            {
                arraylist.add(((ItemStack)object1).copy());
            }
            else if (object1 instanceof Item)
            {
                arraylist.add(new ItemStack((Item)object1));
            }
            else
            {
                if (!(object1 instanceof Block))
                {
                    throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + object1.getClass().getName() + "!");
                }

                arraylist.add(new ItemStack((Block)object1));
            }
        }

        this.recipes.add(new ShapelessRecipes(stack, arraylist));
    }

    /**
     * Adds an IRecipe to the list of crafting recipes.
     *  
     * @param recipe A recipe that will be added to the recipe list.
     */
    public void addRecipe(IRecipe recipe)
    {
        this.recipes.add(recipe);
    }

    /**
     * Retrieves an ItemStack that has multiple recipes for it.
     */
    public ItemStack findMatchingRecipe(InventoryCrafting p_82787_1_, World worldIn)
    {
        Iterator iterator = this.recipes.iterator();
        IRecipe irecipe;

        do
        {
            if (!iterator.hasNext())
            {
                return null;
            }

            irecipe = (IRecipe)iterator.next();
        }
        while (!irecipe.matches(p_82787_1_, worldIn));

        return irecipe.getCraftingResult(p_82787_1_);
    }

    public ItemStack[] func_180303_b(InventoryCrafting p_180303_1_, World worldIn)
    {
        Iterator iterator = this.recipes.iterator();

        while (iterator.hasNext())
        {
            IRecipe irecipe = (IRecipe)iterator.next();

            if (irecipe.matches(p_180303_1_, worldIn))
            {
                return irecipe.getRemainingItems(p_180303_1_);
            }
        }

        ItemStack[] aitemstack = new ItemStack[p_180303_1_.getSizeInventory()];

        for (int i = 0; i < aitemstack.length; ++i)
        {
            aitemstack[i] = p_180303_1_.getStackInSlot(i);
        }

        return aitemstack;
    }

    /**
     * returns the List<> of all recipes
     */
    public List getRecipeList()
    {
        return this.recipes;
    }
}