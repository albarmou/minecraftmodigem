package mod.CraftingManagers.Termociclator3;

import java.util.Comparator;

import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;

public class Termociclator3RecipeSorter implements Comparator
{
	final Termociclator3CraftingManager manager;

	public Termociclator3RecipeSorter(Termociclator3CraftingManager e)
	{
		manager = e;
	}
	
	 private static final String __OBFID = "CL_00000091";
     
	 
	 public int compare(IRecipe p_compare_1_, IRecipe p_compare_2_)
     {
         return p_compare_1_ instanceof Termociclator3ShapelessRecipes && p_compare_2_ instanceof Termociclator3ShapedRecipes ? 1 : (p_compare_2_ instanceof Termociclator3ShapelessRecipes && p_compare_1_ instanceof Termociclator3ShapedRecipes ? -1 : (p_compare_2_.getRecipeSize() < p_compare_1_.getRecipeSize() ? -1 : (p_compare_2_.getRecipeSize() > p_compare_1_.getRecipeSize() ? 1 : 0)));
     }
    
	 public int compare(Object p_compare_1_, Object p_compare_2_)
     {
         return this.compare((IRecipe)p_compare_1_, (IRecipe)p_compare_2_);
     }
	

}
