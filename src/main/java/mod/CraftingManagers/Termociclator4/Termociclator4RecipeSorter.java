package mod.CraftingManagers.Termociclator4;

import java.util.Comparator;

import net.minecraft.item.crafting.IRecipe;

public class Termociclator4RecipeSorter implements Comparator
{
	final Termociclator4CraftingManager manager;

	public Termociclator4RecipeSorter(Termociclator4CraftingManager e)
	{
		manager = e;
	}
	
	 private static final String __OBFID = "CL_00000091";
     
	 
	 public int compare(IRecipe p_compare_1_, IRecipe p_compare_2_)
     {
         return p_compare_1_ instanceof Termociclator4ShapelessRecipes && p_compare_2_ instanceof Termociclator4ShapedRecipes ? 1 : (p_compare_2_ instanceof Termociclator4ShapelessRecipes && p_compare_1_ instanceof Termociclator4ShapedRecipes ? -1 : (p_compare_2_.getRecipeSize() < p_compare_1_.getRecipeSize() ? -1 : (p_compare_2_.getRecipeSize() > p_compare_1_.getRecipeSize() ? 1 : 0)));
     }
    
	 public int compare(Object p_compare_1_, Object p_compare_2_)
     {
         return this.compare((IRecipe)p_compare_1_, (IRecipe)p_compare_2_);
     }
	

}
