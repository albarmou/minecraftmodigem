package mod.CraftingManagers.TermociclatorBioBricks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.world.World;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import AlexIngenieriaBiologica.Elementos.*;

public class TermociclatorBioBricksCraftingManager 
{
	/** The static instance of this class */
    private static TermociclatorBioBricksCraftingManager instance = new TermociclatorBioBricksCraftingManager();
    /** A list of all the recipes added */
    private List recipes;
    private static final String __OBFID = "CL_00000090";

    /**
     * Returns the static instance of this class
     */
    public static TermociclatorBioBricksCraftingManager getInstance()
    {
        /** The static instance of this class */
    	if(instance == null)
    	{
    		instance = new TermociclatorBioBricksCraftingManager();
    	}
        return instance;
    }

    /**
     * AQUI LAS RECETAS M8!!!!
     */
    private TermociclatorBioBricksCraftingManager()
    {
        recipes = new ArrayList();
        
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_1.my_block, 1), new Object[]{ "XY", 'X', Element_Lac01.my_block, 'Y', Element_RBS.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_2.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_1.my_block, 'Y', Element_tetR_Lite.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_3.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_2.my_block, 'Y', Element_T1Terminator.my_block.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_4.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_3.my_block, 'Y', Element_LambdaPr.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_5.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_4.my_block, 'Y', Element_RBS.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_6.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_5.my_block, 'Y', Element_LacI_Lite.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_7.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_6.my_block, 'Y', Element_T1Terminator.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_8.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_7.my_block, 'Y', Element_Tet01.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_9.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_8.my_block, 'Y', Element_RBS.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_10.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_9.my_block, 'Y', Element_LambdaC1_Lite.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_11.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_10.my_block, 'Y', Element_T1Terminator.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_12.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_11.my_block, 'Y', Element_Tet01.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_13.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_12.my_block, 'Y', Element_RBS.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_14.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_13.my_block, 'Y', Element_GFP_AAV.my_block});
        this.addRecipe(new ItemStack(Element_Composite_Repressilator_Final.my_block, 1), new Object[]{"XY", 'X', Element_Composite_Repressilator_12.my_block, 'Y', Element_T1Terminator.my_block});

    	Collections.sort(this.recipes, new TermociclatorBioBricksRecipeSorter(this));
    }

    /**
     * Adds a shaped recipe to the games recipe list.
     */
    public TermociclatorBioBricksShapedRecipes addRecipe(ItemStack stack, Object ... recipeComponents)
    {
        String s = "";
        int i = 0;
        int j = 0;
        int k = 0;

        if (recipeComponents[i] instanceof String[])
        {
            String[] astring = (String[])((String[])recipeComponents[i++]);

            for (int l = 0; l < astring.length; ++l)
            {
                String s1 = astring[l];
                ++k;
                j = s1.length();
                s = s + s1;
            }
        }
        else
        {
            while (recipeComponents[i] instanceof String)
            {
                String s2 = (String)recipeComponents[i++];
                ++k;
                j = s2.length();
                s = s + s2;
            }
        }

        HashMap hashmap;

        for (hashmap = Maps.newHashMap(); i < recipeComponents.length; i += 2)
        {
            Character character = (Character)recipeComponents[i];
            ItemStack itemstack1 = null;

            if (recipeComponents[i + 1] instanceof Item)
            {
                itemstack1 = new ItemStack((Item)recipeComponents[i + 1]);
            }
            else if (recipeComponents[i + 1] instanceof Block)
            {
                itemstack1 = new ItemStack((Block)recipeComponents[i + 1], 1, 32767);
            }
            else if (recipeComponents[i + 1] instanceof ItemStack)
            {
                itemstack1 = (ItemStack)recipeComponents[i + 1];
            }

            hashmap.put(character, itemstack1);
        }

        ItemStack[] aitemstack = new ItemStack[j * k];

        for (int i1 = 0; i1 < j * k; ++i1)
        {
            char c0 = s.charAt(i1);

            if (hashmap.containsKey(Character.valueOf(c0)))
            {
                aitemstack[i1] = ((ItemStack)hashmap.get(Character.valueOf(c0))).copy();
            }
            else
            {
                aitemstack[i1] = null;
            }
        }

        TermociclatorBioBricksShapedRecipes shapedrecipes = new TermociclatorBioBricksShapedRecipes(j, k, aitemstack, stack);
        this.recipes.add(shapedrecipes);
        return shapedrecipes;
    }

    /**
     * Adds a shapeless crafting recipe to the the game.
     *  
     * @param recipeComponents An array of ItemStack's Item's and Block's that make up the recipe.
     */
    public void addTermociclatorBioBricksShapelessRecipe(ItemStack stack, Object ... recipeComponents)
    {
        ArrayList arraylist = Lists.newArrayList();
        Object[] aobject = recipeComponents;
        int i = recipeComponents.length;

        for (int j = 0; j < i; ++j)
        {
            Object object1 = aobject[j];

            if (object1 instanceof ItemStack)
            {
                arraylist.add(((ItemStack)object1).copy());
            }
            else if (object1 instanceof Item)
            {
                arraylist.add(new ItemStack((Item)object1));
            }
            else
            {
                if (!(object1 instanceof Block))
                {
                    throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + object1.getClass().getName() + "!");
                }

                arraylist.add(new ItemStack((Block)object1));
            }
        }

        this.recipes.add(new ShapelessRecipes(stack, arraylist));
    }

    /**
     * Adds an IRecipe to the list of crafting recipes.
     *  
     * @param recipe A recipe that will be added to the recipe list.
     */
    public void addRecipe(IRecipe recipe)
    {
        this.recipes.add(recipe);
    }

    /**
     * Retrieves an ItemStack that has multiple recipes for it.
     */
    public ItemStack findMatchingRecipe(InventoryCrafting p_82787_1_, World worldIn)
    {
        Iterator iterator = this.recipes.iterator();
        IRecipe irecipe;

        do
        {
            if (!iterator.hasNext())
            {
                return null;
            }

            irecipe = (IRecipe)iterator.next();
        }
        while (!irecipe.matches(p_82787_1_, worldIn));

        return irecipe.getCraftingResult(p_82787_1_);
    }

    public ItemStack[] func_180303_b(InventoryCrafting p_180303_1_, World worldIn)
    {
        Iterator iterator = this.recipes.iterator();

        while (iterator.hasNext())
        {
            IRecipe irecipe = (IRecipe)iterator.next();

            if (irecipe.matches(p_180303_1_, worldIn))
            {
                return irecipe.getRemainingItems(p_180303_1_);
            }
        }

        ItemStack[] aitemstack = new ItemStack[p_180303_1_.getSizeInventory()];

        for (int i = 0; i < aitemstack.length; ++i)
        {
            aitemstack[i] = p_180303_1_.getStackInSlot(i);
        }

        return aitemstack;
    }

    /**
     * returns the List<> of all recipes
     */
    public List getRecipeList()
    {
        return this.recipes;
    }
}
