package mod.CraftingManagers.TermociclatorBioBricks;

import java.util.Comparator;

import net.minecraft.item.crafting.IRecipe;

public class TermociclatorBioBricksRecipeSorter implements Comparator
{
	final TermociclatorBioBricksCraftingManager manager;

	public TermociclatorBioBricksRecipeSorter(TermociclatorBioBricksCraftingManager e)
	{
		manager = e;
	}
	
	 private static final String __OBFID = "CL_00000091";
     
	 
	 public int compare(IRecipe p_compare_1_, IRecipe p_compare_2_)
     {
         return p_compare_1_ instanceof TermociclatorBioBricksShapelessRecipes && p_compare_2_ instanceof TermociclatorBioBricksShapedRecipes ? 1 : (p_compare_2_ instanceof TermociclatorBioBricksShapelessRecipes && p_compare_1_ instanceof TermociclatorBioBricksShapedRecipes ? -1 : (p_compare_2_.getRecipeSize() < p_compare_1_.getRecipeSize() ? -1 : (p_compare_2_.getRecipeSize() > p_compare_1_.getRecipeSize() ? 1 : 0)));
     }
    
	 public int compare(Object p_compare_1_, Object p_compare_2_)
     {
         return this.compare((IRecipe)p_compare_1_, (IRecipe)p_compare_2_);
     }
}
