package mod.CraftingManagers.Termociclator5;

import java.util.Comparator;
import net.minecraft.item.crafting.IRecipe;

public class Termociclator5RecipeSorter implements Comparator
{
	final Termociclator5CraftingManager manager;

	public Termociclator5RecipeSorter(Termociclator5CraftingManager e)
	{
		manager = e;
	}
	
	 private static final String __OBFID = "CL_00000091";
     
	 
	 public int compare(IRecipe p_compare_1_, IRecipe p_compare_2_)
     {
         return p_compare_1_ instanceof Termociclator5ShapelessRecipes && p_compare_2_ instanceof Termociclator5ShapedRecipes ? 1 : (p_compare_2_ instanceof Termociclator5ShapelessRecipes && p_compare_1_ instanceof Termociclator5ShapedRecipes ? -1 : (p_compare_2_.getRecipeSize() < p_compare_1_.getRecipeSize() ? -1 : (p_compare_2_.getRecipeSize() > p_compare_1_.getRecipeSize() ? 1 : 0)));
     }
    
	 public int compare(Object p_compare_1_, Object p_compare_2_)
     {
         return this.compare((IRecipe)p_compare_1_, (IRecipe)p_compare_2_);
     }
	

}
