package mod.flower;

import AlexIngenieriaBiologica.Elementos.Element_Final_Final_Circuit;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import mod.Reference;
import mod.init.LuzAzul;
import mod.init.LuzRoja;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MyFlower extends Block 
{
	/*
	private int tN = 1;
	private boolean touched = false;
	
	private BiologicalElement input = null;
	private int index = 0;
	private int []luzTouched = new int[2];
	
	*/
	private static String name;
	
	public MyFlower(String name) 
	{
		super(Material.glass);
		this.name = name;
	}

	private static MyFlower my_block;
	
	public static void init()
	{
		my_block = (MyFlower) new MyFlower(name).setUnlocalizedName("DNA_Flower");
		my_block.setHardness(1F);
		my_block.setResistance(1F);
		/*
		red.setDefaultState(red.blockState.getBaseState());//new BlockState(red, new IProperty[]{}));
		white.setDefaultState(white.blockState.getBaseState());//(IBlockState) new BlockState(white, new IProperty[]{}));*/
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}

	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	@Override
	public BlockState createBlockState()
	{
		return new BlockState(this, new IProperty[]{});
	}
	
	@SideOnly(Side.CLIENT)
	@Override
    public EnumWorldBlockLayer getBlockLayer()
    {
        return EnumWorldBlockLayer.CUTOUT_MIPPED;
    }
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
        return super.onBlockPlaced(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer);
    }
		
	public String toString()
	{
		return name.replace("_", " ");
	}
}
