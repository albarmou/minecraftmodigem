package mod.flower.whiteVariants;

import mod.Reference;
import mod.flower.MyFlower;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class WhiteFlowerIncorrect extends MyFlower
{
	public static WhiteFlowerIncorrect my_block;
	
	public static String name = "dna_flower_white_i";

	private boolean touched = false;
	
	public WhiteFlowerIncorrect()
	{
		super(name);
	}
	
	public static void init()
	{
		my_block = (WhiteFlowerIncorrect) new WhiteFlowerIncorrect().setUnlocalizedName(name);
		my_block.setHardness(2F);
		my_block.setResistance(2F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		return true;
    }
	
	//Cuando boton izquierdo
	@Override
    public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn) 
	{
		//empieza con false
		this.touched = !this.touched;
		
		if(this.touched)
		{
			mod.Util.sendChatMessageAsMod(this.toString());
		}
	}
	
	@Override
	public String toString()
	{
		return "Incorrect input";
	}
}
