package mod.flower.whiteVariants;

import mod.Reference;
import mod.Jeringuillas.JeringuillaBuena;
import mod.Jeringuillas.JeringuillaMala;
import mod.Jeringuillas.JeringuillaVacia;
import mod.flower.BlueFlower;
import mod.flower.GreenFlower;
import mod.flower.MyFlower;
import mod.flower.RedFlower;
import mod.flower.SexyPlant;
import mod.flower.YellowFlower;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Elementos.Element_Composite_Repressilator_Final;
import AlexIngenieriaBiologica.Elementos.Element_SEXY_DNA;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class WhiteFlower extends MyFlower
{
	public static WhiteFlower my_block;
	
	private int tN = 1;
	private boolean touched = false;
	
	private BiologicalElement input = null;
	private int index = 0;
	
	private final int ArraySize = 2;
	
	private boolean hasShownInfo = false;
	
	private int []luzTouched = new int[ArraySize];
	
	private static String name = "dna_flower_white";
	
	public WhiteFlower(String name) 
	{
		super(name);
	}
	
	public static void init()
	{
		my_block = (WhiteFlower) new WhiteFlower(name).setUnlocalizedName("dna_flower_white");
		my_block.setResistance(2F);
		my_block.setHardness(2F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	/*@Override
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
    {
		this.input = null;
		this.luzTouched = new int[ArraySize];
		this.index = 0;
        return super.onBlockPlaced(worldIn, pos, facing, hitX, hitY, hitZ, meta, placer);
    }*/
	
	private void manageChanges(Item luz, World world, EntityPlayer player, BlockPos pos)
	{
		if(index < this.luzTouched.length)
		{
			if(luz.getUnlocalizedName().contains("roja"))//no va instanceof??? xk?
			{
				this.luzTouched[index++] = 0;
				mod.Util.sendChatMessageAsMod("Asignado toque " + (index - 1) + " a " + "rojo.");
			}
			else if(luz.getUnlocalizedName().contains("azul"))
			{
				this.luzTouched[index++] = 1;
				mod.Util.sendChatMessageAsMod("Asignado toque " + (index - 1) + " a " + "azul.");
			}
		}	
			//Minecraft.getMinecraft().thePlayer.sendChatMessage("Hecho");
		if(index == this.luzTouched.length)
		{
			Block b = getResult();
			
			if(b != null && b instanceof MyFlower)
			{
				MyFlower e = (MyFlower)b;
				//cambio
				world.destroyBlock(pos, false);
				//player.dropPlayerItemWithRandomChoice(new ItemStack(Item.getItemFromBlock(b)), true);
				world.setBlockState(pos, e.getDefaultState());
			}
			else
			{
				mod.Util.sendChatMessageAsMod("No hay resultado para esta combinacion.");
			}
		}
		
	}
	
	private Block getResult()
	{
		//0 = rojo
		//1 = azul
		Block output = null;
		
		if(this.input instanceof PoweredElement && this.input.getUnlocalizedName().substring(5).contains("Final_Final"))
		{
			if(this.luzTouched[0] == 0)
			{
				if(this.luzTouched[1] == 0)
				{
					//azul
					mod.Util.sendChatMessageAsMod("azul");
					output = BlueFlower.my_block;
				}
				else if(this.luzTouched[1] == 1)
				{
					//amarillo
					mod.Util.sendChatMessageAsMod("amarillo");
					output = YellowFlower.my_block;
				}
			}
			else if(this.luzTouched[0] == 1)
			{
				if(this.luzTouched[1] == 0)
				{
					//rojo
					mod.Util.sendChatMessageAsMod("rojo");
					output = RedFlower.my_block;
				}
				else if(this.luzTouched[1] == 1)
				{
					//verde
					mod.Util.sendChatMessageAsMod("verde");
					output = GreenFlower.my_block;
				}
			}
		}
		else
		{
			mod.Util.sendChatMessageAsMod("No tiene un powered element correcto dentro");
		}
		
		return output;
	}
	
	private void manageChanges(Block element, World world, EntityPlayer player)
	{
		if(this.input == null)
		{
			player.inventory.consumeInventoryItem(Item.getItemFromBlock(element));
			this.input = (BiologicalElement) element;
			mod.Util.sendChatMessageAsMod("Asignado " + this.input.getUnlocalizedName());
		}
		else
		{
			//player.dropPlayerItemWithRandomChoice(new ItemStack(Item.getItemFromBlock(this.input)), true);
			//this.input = (BiologicalElement) element;
			mod.Util.sendChatMessageAsMod("Ya tiene uno tio");
		}
		
	}
	
	//Cuando boton derecho
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		if(playerIn.getHeldItem() == null)
		{
			tN = 1;
			return false;//MAL, cambiar plz!!!!!!!!!
		}
		
		ItemStack stack = playerIn.getHeldItem();
		
		if(stack != null)
		{
			Item held = stack.getItem();
			
			Block blockHeld = Block.getBlockFromItem(held);
			/*
			if(blockHeld != null && blockHeld instanceof BiologicalElement)
			{
				playerIn.inventory.consumeInventoryItem(held);

				mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
				
				if(blockHeld instanceof PoweredElement)
				{
					worldIn.setBlockState(pos, WhiteFlowerCorrect.my_block.getDefaultState());
				}
				else
				{
					worldIn.setBlockState(pos, WhiteFlowerIncorrect.my_block.getDefaultState());
				}
			
			*/
		
			playerIn.inventory.consumeInventoryItem(held);
			
			if(held instanceof JeringuillaBuena)
			{
				mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
				worldIn.setBlockState(pos, WhiteFlowerCorrect.my_block.getDefaultState());
			}
			else if(held instanceof JeringuillaMala)
			{
				mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_ANGRY);
				worldIn.setBlockState(pos, WhiteFlowerIncorrect.my_block.getDefaultState());
			}
			else if(held instanceof JeringuillaVacia)
			{
				mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_ANGRY);
				worldIn.setBlockState(pos, WhiteFlowerIncorrect.my_block.getDefaultState());
			}
			else if(blockHeld instanceof Element_SEXY_DNA)
			{
				mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
				worldIn.setBlockState(pos, SexyPlant.my_block.getDefaultState());
			}
		}

		tN = 1;		
		return true;
    }
	
	//Cuando boton izquierdo
	@Override
    public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn) 
	{
		//empieza con false
		this.hasShownInfo = !this.hasShownInfo;
		
		if(this.hasShownInfo)
		{
			mod.Util.sendChatMessageAsMod(this.toString());
		}
	}
	
	@Override
	public String toString()
	{
		String nombre = name;
		String nInput = "";
		String tocado = "";
		
		if(this.input != null)
		{
			nInput = "(" + this.input.GetName() + ", Powered: " + this.input.hasBeenPowered() + ")";
		}
		else
		{
			nInput = "()";
		}
		
		if(this.touched)
		{
			tocado = "()";
		}
		else
		{
			//0 = rojo
			//1 = azul
			tocado = "(";
			for(int i = 0; i < this.index; i++)
			{
				tocado += i + ": ";
				
				if(this.luzTouched[0] == 0)
				{
					tocado += "rojo,";
				}
				else
				{
					tocado += "azul,";
				}
			}
			
			if(this.index != 0)
				tocado = tocado.substring(0, tocado.length() - 1);
			
			tocado += ")";
		}
		
		
		return nombre + ", input:" + nInput + ", light:" + tocado;
	}
}
