package mod.flower.whiteVariants;

import mod.Reference;
import mod.flower.GreenFlower;
import mod.flower.MyFlower;
import mod.flower.RedFlower;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class WhiteFlowerCorrectBlue extends MyFlower
{
	public static WhiteFlowerCorrectBlue my_block;
	
	public static String name = "dna_flower_white_c_b";

	private boolean touched = false;
	
	public WhiteFlowerCorrectBlue()
	{
		super(name);
	}
	
	public static void init()
	{
		my_block = (WhiteFlowerCorrectBlue) new WhiteFlowerCorrectBlue().setUnlocalizedName(name);
		my_block.setHardness(2F);
		my_block.setResistance(2F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		ItemStack stack = playerIn.getHeldItem();
		
		if(stack != null)
		{
			Item heldItem = stack.getItem();
			
			if(heldItem.getUnlocalizedName().contains("luz"))
			{
				mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
				
				if(heldItem.getUnlocalizedName().contains("roja"))
				{
					worldIn.setBlockState(pos, RedFlower.my_block.getDefaultState());
				}
				else if(heldItem.getUnlocalizedName().contains("azul"))
				{
					worldIn.setBlockState(pos, GreenFlower.my_block.getDefaultState());
				}
			}
		}
				
		return true;		
    }
	
	//Cuando boton izquierdo
	@Override
    public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn) 
	{
		//empieza con false
		this.touched = !this.touched;
		
		if(this.touched)
		{
			mod.Util.sendChatMessageAsMod(this.toString());
		}
	}
	
	@Override
	public String toString()
	{
		return "Input: (Correct) - Light Input: Blue";
	}
}
