package mod.flower;

import mod.Reference;
import mod.Util;
import mod.Enfermedades.ColeraPotionEffect;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class SexyPlant extends MyFlower
{
	public static SexyPlant my_block;
	
	public SexyPlant()
	{
		super("SexyPlant");
	}
	

	public static void init()
	{
		my_block = (SexyPlant) new SexyPlant().setUnlocalizedName("SexyPlant");
		my_block.setResistance(2F);
		my_block.setHardness(2F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.HEART);
		
		return true;
    }
}
