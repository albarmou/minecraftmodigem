package mod.flower;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlueFlower extends MyFlower
{
	public static BlueFlower my_block;
	private static String name = "DNA_Flower_blue";
	
	public BlueFlower(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public static void init()
	{
		my_block = (BlueFlower) new BlueFlower(BlueFlower.name).setUnlocalizedName(name);
		my_block.setHardness(3F);
		my_block.setResistance(3F);
	}

	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
