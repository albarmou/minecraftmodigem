package mod.flower;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class RedFlower extends MyFlower
{
	public static RedFlower my_block;
	private static String name = "DNA_Flower_red";
	
	public RedFlower(String name) 
	{
		super(name);
	}
	
	public static void init()
	{
		my_block = (RedFlower) new RedFlower(name).setUnlocalizedName("DNA_Flower_red");
		my_block.setResistance(2F);
		my_block.setHardness(2F);
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
