package mod;

public class Reference 
{
	public static final String MOD_ID = "aladDNA";
	public static final String MOD_NAME= "aladDNA";
	public static final String VERSION = "1.6";
	
	public static final int GUIID_Termociclator3 = 1;
	public static final int GUIID_Termociclator2 = 2;
	public static final int GUIID_TermociclatorBioBricks = 3;
	public static final int GUIID_Termociclator4 = 4;
	public static final int GUIID_Termociclator5 = 5;
	public static final int GUIID_Electropolador = 6;
	
	/**
	 * Direccion a nuestra clase cliente proxy dentro del proyecto
	 */
	public static final String CLIENT_PROXY_CLASS = "mod.proxy.ClientProxy";
	
	/**
	 * Direccion a nuestra clase common (servidor) proxy dentro del proyecto
	 */
	public static final String COMMON_PROXY_CLASS = "mod.proxy.CommonProxy";
}
