package mod.GUI;

import mod.Reference;
import mod.container.ContainerTermociclatorBioBricks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

public class GUITermociclatorBioBricks extends GuiContainer
{
	//usamos el mismo ya que son iguales podemos hacer una propia en el futuro cambiando esto
	private ResourceLocation resource = new ResourceLocation(Reference.MOD_ID + ":" + "textures/GUI/Termociclator2.png"); 
	
	public GUITermociclatorBioBricks(InventoryPlayer invPlayer, World world, int x, int y, int z)
	{
		super(new ContainerTermociclatorBioBricks(invPlayer, world, x, y, z));
		
		this.xSize = 175;
		this.ySize = 187;		
	}

	public void onGuiClosed()
	{
		super.onGuiClosed();
	}
	
	protected void drawGuiContainerforegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal("Work Surface"), 20, 20, 0x000000);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1F, 1F, 1F, 1F);
		
		Minecraft.getMinecraft().getTextureManager().bindTexture(resource);
		
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
	}
}
