package mod.GUI;

import mod.Reference;
import mod.container.ContainerElectropolador;
import mod.container.ContainerTermociclator2;
import mod.container.ContainerTermociclator3;
import mod.container.ContainerTermociclator4;
import mod.container.ContainerTermociclator5;
import mod.container.ContainerTermociclatorBioBricks;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GUIHandler implements  IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) 
	{
		TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));
		
		if(entity != null)
		{
			switch(ID)
			{
				default:
					return null;
			}
		}
		
		switch(ID)
		{
			case Reference.GUIID_Termociclator3:
				return new ContainerTermociclator3(player.inventory, world, x, y, z);
			case Reference.GUIID_Termociclator2:
				return new ContainerTermociclator2(player.inventory, world, x, y, z);
			case Reference.GUIID_TermociclatorBioBricks:
				return new ContainerTermociclatorBioBricks(player.inventory, world, x, y, z);
			case Reference.GUIID_Termociclator4:
				return new ContainerTermociclator4(player.inventory, world, x, y, z);
			case Reference.GUIID_Termociclator5:
				return new ContainerTermociclator5(player.inventory, world, x, y, z);
			case Reference.GUIID_Electropolador:
				return new ContainerElectropolador(player.inventory, world, x, y, z);
			default:
				break;
		}
		
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch(ID)
		{
			case Reference.GUIID_Termociclator3:
				return new GUITermociclator3(player.inventory, world, x, y, z);
			case Reference.GUIID_Termociclator2:
				return new GUITermociclator2(player.inventory, world, x, y, z);
			case Reference.GUIID_TermociclatorBioBricks:
				return new GUITermociclatorBioBricks(player.inventory, world, x, y, z);
			case Reference.GUIID_Termociclator4:
				return new GUITermociclator4(player.inventory, world, x, y, z);
			case Reference.GUIID_Termociclator5:
				return new GUITermociclator5(player.inventory, world, x, y, z);
			case Reference.GUIID_Electropolador:
				return new GUIElectropolador(player.inventory, world, x, y, z);
			default:
				break;
		}
		
		return null;
	}

}
