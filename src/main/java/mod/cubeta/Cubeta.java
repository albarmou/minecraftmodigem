package mod.cubeta;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;

public class Cubeta extends Block
{
	protected BiologicalElement contenido;
	
	public Cubeta()
	{
		super(Material.glass);
	}
	
	public void setContenido(BiologicalElement input)
	{
		//si ya tiene uno se sustituye????
		this.contenido = input;
	}
	
	public Cubeta getResult(BiologicalElement input)
	{
		if(input.hasBeenPowered())
		{
			if(input.isFinal())
			{
				//bueno
			}
			else
			{
				//malo
			}
		}
		else
		{
			//vacio
		}
		
		return null;
	}
	
	//Cuando boton derecho
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {		
		ItemStack heldStack = playerIn.getHeldItem();
		
		if(heldStack != null)
		{
			if(Block.getBlockFromItem(heldStack.getItem()) instanceof BiologicalElement)
			{
				BiologicalElement input = (BiologicalElement)Block.getBlockFromItem(heldStack.getItem());
				
				worldIn.destroyBlock(pos, false);
				mod.Util.sendChatMessageAsMod("Asignado " + input.GetName());
				playerIn.dropItem(Item.getItemFromBlock(getResult(input)), 1);
			}
		}
		else
		{
			return false;
		}
		
		return true;
    }
}
