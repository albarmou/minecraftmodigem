package mod.container;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class ContainerElectropolador extends Container
{
	public InventoryCrafting craftMatrix;
	public IInventory craftResult;
	private World worldObject;
	private int posX;
	private int posY;
	private int posZ;
	
	public ContainerElectropolador(InventoryPlayer p, World w, int x, int y, int z)
	{
		int sizex = 16;
		int sizey = 16;
		int initx = 44;
		int inity = 43;
		int border = 2;
		
		craftMatrix = new InventoryCrafting(this, 1, 1);
		craftResult = new InventoryCraftResult();
		worldObject = w;
		posX = x;
		posY = y;
		posZ = z;
		
		this.addSlotToContainer(new SlotCrafting(p.player, craftMatrix, craftResult, 0, 141, 43));
		
		//a�adimos grids
		for(int i = 0; i < craftMatrix.getHeight(); i++)
		{
			for(int j = 0; j < craftMatrix.getWidth(); j++)
			{
				this.addSlotToContainer(new Slot(craftMatrix, j + i * craftMatrix.getWidth(), initx + (j * (sizex + border)), inity + i * 18));
			}
		}
		
		//a�adir resto de celdas - no necesario si no ponemos mas
		//inventory slots
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				this.addSlotToContainer(new Slot(p, j + i * 9 + 9, 8 + j * 18, 106 + i * 18));
			}
		}
		
		//player hotbar
		for(int i = 0; i < 9; i++)
		{
			this.addSlotToContainer(new Slot(p, i, 8 + i * 18, 164));
		}
		
		onCraftMatrixChanged(craftMatrix);
	}
	
	public void onCraftMatrixChanged(IInventory inventoryIn)
	{
		if(this.craftMatrix.getStackInSlot(0) != null)
		{
			Item input = this.craftMatrix.getStackInSlot(0).getItem();
			
			if(input != null && Block.getBlockFromItem(input) instanceof BiologicalElement && !(Block.getBlockFromItem(input) instanceof PoweredElement))
			{
				BiologicalElement inputBio = (BiologicalElement)Block.getBlockFromItem(input);
				
				this.craftResult.setInventorySlotContents(0, new ItemStack(inputBio.getPoweredVersion(), this.craftMatrix.getStackInSlot(0).stackSize));
				this.craftMatrix.setInventorySlotContents(0, null);
			}
		}
	}
	
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2) 
	{
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(par2);
        int omg = 9 - (craftMatrix.getWidth() * craftMatrix.getHeight());

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (par2 == 0)
            {
                if (!this.mergeItemStack(itemstack1, 10 - omg, 46- omg, false))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (par2 >= 10- omg && par2 < 37- omg)
            {
                if (!this.mergeItemStack(itemstack1, 37 - omg, 46 - omg, false))
                {
                    return null;
                }
            }
            else if (par2 >= 37- omg && par2 < 46- omg)
            {
                if (!this.mergeItemStack(itemstack1, 10 - omg, 37 - omg, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 10- omg, 46- omg, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }

        return itemstack;
    }
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) 
	{
		return playerIn.getDistanceSq((double)posX, (double)posY, (double)posZ) <= 64.00;
	}
}
