package mod.container;

import java.util.ListIterator;

import mod.CraftingManagers.Termociclator3.Termociclator3CraftingManager;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.world.World;
import AlexIngenieriaBiologica.Reference.ElementType;
import AlexIngenieriaBiologica.Tipos.ADN_Malo;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;

public class ContainerTermociclator3 extends Container 
{
	public InventoryCrafting craftMatrix;
	public IInventory craftResult;
	private World worldObject;
	private int posX;
	private int posY;
	private int posZ;
	
	public ContainerTermociclator3(InventoryPlayer p, World w, int x, int y, int z)
	{
		int sizex = 16;
		int sizey = 16;
		int initx = 26;
		int inity = 43;
		int border = 2;
		
		craftMatrix = new InventoryCrafting(this, 3, 1);
		craftResult = new InventoryCraftResult();
		worldObject = w;
		posX = x;
		posY = y;
		posZ = z;
		
		this.addSlotToContainer(new SlotCrafting(p.player, craftMatrix, craftResult, 0, 141, 43));
		
		//a�adimos grids
		for(int i = 0; i < craftMatrix.getHeight(); i++)
		{
			for(int j = 0; j < craftMatrix.getWidth(); j++)
			{
				this.addSlotToContainer(new Slot(craftMatrix, j + i * craftMatrix.getWidth(), initx + (j * (sizex + border)), inity + i * 18));
			}
		}
		
		//a�adir resto de celdas - no necesario si no ponemos mas
		//inventory slots
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				this.addSlotToContainer(new Slot(p, j + i * 9 + 9, 8 + j * 18, 106 + i * 18));
			}
		}
		
		//player hotbar
		for(int i = 0; i < 9; i++)
		{
			this.addSlotToContainer(new Slot(p, i, 8 + i * 18, 164));
		}
		
		onCraftMatrixChanged(craftMatrix);
	}
	
	/**
     * Returns true if automation is allowed to insert the given stack (ignoring stack size) into the given slot.
     */
	public boolean isItemValidForSlot(int index, ItemStack stack)
    {
    	boolean bio = Block.getBlockFromItem(stack.getItem()) instanceof BiologicalElement;
        BiologicalElement block = null;
        
    	if(bio)
    	{
    		block = ((BiologicalElement)Block.getBlockFromItem(stack.getItem()));
    		Minecraft.getMinecraft().thePlayer.sendChatMessage(block.GetTipe().toString() + " " + index);
    		return index == 0 ? (block.GetTipe() == ElementType.PROMOTER) : index == 1 ? (block.GetTipe() == ElementType.CDS) : index == 2 ? (block.GetTipe() == ElementType.TERMINATOR) : true;
    	}
    	else
    	{
    		return false;
    	}
    	
    }
	/**
     * Handles slot click. Args : slotId, clickedButton, mode (0 = basic click, 1 = shift click, 2 = Hotbar, 3 =
     * pickBlock, 4 = Drop, 5 = ?, 6 = Double click), player
     */
	@Override
    public ItemStack slotClick(int slotId, int clickedButton, int mode, EntityPlayer playerIn)
    {
    	ItemStack output = super.slotClick(slotId, clickedButton, mode, playerIn);
    	
    	//slotId == 0 es result
    	if(slotId == 0)
    	{
    		if(clickedButton == 0 && mode == 0)
    		{
    			consumeItemsOnCreation();
    		}
    	}
    	if(output != null)
    	{
    		//System.out.println(output.toString() + " <> " + output.equals(playerIn.getHeldItem()) + " <> ");
	    	//playerIn.inventory.setInventorySlotContents(clickedButton, output);
    	}
    	else
    		System.out.println("Es null!");
    	
    	return output;
    }

    private void consumeItemsOnCreation()
    {
		for(int i = 0; i < this.craftMatrix.getWidth(); i++)
		{
			if(this.craftMatrix.getStackInSlot(i) != null)
			{
				this.craftMatrix.decrStackSize(i, 1);					
			}
		}
    }
	
	 /**
     * Called when the container is closed.
     */
    public void onContainerClosed(EntityPlayer playerIn)
    {
        super.onContainerClosed(playerIn);

        if (!this.worldObject.isRemote)
        {
            for (int i = 0; i < this.craftMatrix.getWidth() * this.craftMatrix.getHeight(); ++i)
            {
                ItemStack itemstack = this.craftMatrix.getStackInSlotOnClosing(i);

                if (itemstack != null)
                {
                    playerIn.dropPlayerItemWithRandomChoice(itemstack, false);
                }
            }
        }
    }
	
	/**
     * Callback for when the crafting matrix is changed.
     */
    public void onCraftMatrixChanged(IInventory inventoryIn)
    {
    	boolean allow = false;
    	boolean algunoNull = false;
    	boolean algunoNoBio = false;

    	if(this.craftMatrix.getStackInSlot(0) == null || this.craftMatrix.getStackInSlot(1) == null || this.craftMatrix.getStackInSlot(2) == null)
    	{
    		//??? wut m8
    		algunoNull = true;
    	}
    	else 
    	{	
	    	if(Block.getBlockFromItem(this.craftMatrix.getStackInSlot(0).getItem()) instanceof BiologicalElement && Block.getBlockFromItem(this.craftMatrix.getStackInSlot(1).getItem()) instanceof BiologicalElement && Block.getBlockFromItem(this.craftMatrix.getStackInSlot(2).getItem()) instanceof BiologicalElement)
	    	{
	    		BiologicalElement aux = (BiologicalElement)Block.getBlockFromItem(this.craftMatrix.getStackInSlot(0).getItem());
	    		if(aux.GetTipe() == ElementType.PROMOTER)
	    		{
	    			aux = (BiologicalElement)Block.getBlockFromItem(this.craftMatrix.getStackInSlot(1).getItem());
	    			
	    			if(aux.GetTipe() == ElementType.CDS)
	    			{
	    				aux = (BiologicalElement)Block.getBlockFromItem(this.craftMatrix.getStackInSlot(2).getItem());
	    				
	    				if(aux.GetTipe() == ElementType.TERMINATOR)
	    				{
	    					allow = true;
	    				}
	    			}
	    		}
	    	}
	    	else
	    	{
	    		algunoNoBio = true;
	    	}
	    	
	    	
    	}
    	    	
    	if(algunoNull || algunoNoBio)
    	{
    		this.craftResult.setInventorySlotContents(0, null);
    	}
    	else if(allow)
    	{
    		ItemStack result = Termociclator3CraftingManager.getInstance().findMatchingRecipe(this.craftMatrix, this.worldObject);
	    	
			this.craftResult.setInventorySlotContents(0, result);
    	}
    	else
    	{
    		//son bio pero no en orden
    		ItemStack result = Termociclator3CraftingManager.getInstance().findMatchingRecipe(this.craftMatrix, this.worldObject);
	    	
	    	if(result == null)
	    	{
	    		result = new ItemStack(ADN_Malo.my_block, 1);
	    	}
	    	
			this.craftResult.setInventorySlotContents(0, result);
    	}
    }

	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2) 
	{
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(par2);
        int omg = 9 - (craftMatrix.getWidth() * craftMatrix.getHeight());

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (par2 == 0)
            {
                if (!this.mergeItemStack(itemstack1, 10 - omg, 46- omg, false))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (par2 >= 10- omg && par2 < 37- omg)
            {
                if (!this.mergeItemStack(itemstack1, 37 - omg, 46 - omg, false))
                {
                    return null;
                }
            }
            else if (par2 >= 37- omg && par2 < 46- omg)
            {
                if (!this.mergeItemStack(itemstack1, 10 - omg, 37 - omg, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 10- omg, 46- omg, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }

        return itemstack;
    }
    
	/**
     * Take a stack from the specified inventory slot.
     * Tal cual de las de minecraft
     *//*
    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);
        int omg = 9 - (craftMatrix.getWidth() * craftMatrix.getHeight());

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();            

            if (index == 0)
            {
                if (!this.mergeItemStack(itemstack1, 37 - omg, 46 - omg, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }
            else if (index >= 10 && index < 37)
            {
                if (!this.mergeItemStack(itemstack1, 37 - omg, 46 - omg, false))
                {
                    return null;
                }
            }
            else if (index >= 37 && index < 46)
            {
                if (!this.mergeItemStack(itemstack1, 10 - omg, 37, false))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 10 - omg, 46 - omg, false))
            {
                return null;
            }



            
            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }
            
            
            slot.onPickupFromSlot(playerIn, itemstack1);
            
            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
        }

        return itemstack;
    }
	*/
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) 
	{
		return playerIn.getDistanceSq((double)posX, (double)posY, (double)posZ) <= 64.00;
	}

}
