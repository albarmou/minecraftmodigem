package mod.PlacasPetri;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class PlacaPetriAbstract extends Block
{
	public static PlacaPetriAbstract my_block;
	
	public PlacaPetriAbstract()
	{
		super(Material.glass);
	}
	
	public static void init()
	{
		my_block = (PlacaPetriAbstract) new PlacaPetriAbstract().setUnlocalizedName("placa_abs");
	}
	
	//Cuando boton derecho
	@Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		return true;
    }
	
	public PlacaPetriAbstract GetResult(BiologicalElement input)
	{
		return this;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
    public EnumWorldBlockLayer getBlockLayer()
    {
        return EnumWorldBlockLayer.CUTOUT_MIPPED;
    }
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
}
