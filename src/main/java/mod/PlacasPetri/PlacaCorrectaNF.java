package mod.PlacasPetri;

import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class PlacaCorrectaNF extends PlacaPetriAbstract
{
	public static PlacaCorrectaNF my_block;
	
	public PlacaCorrectaNF()
	{
		super();
	}
	
	public static void init()
	{
		my_block = (PlacaCorrectaNF) new PlacaCorrectaNF().setUnlocalizedName("placa_correcta_noFinal");
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
