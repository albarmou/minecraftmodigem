package mod.PlacasPetri;

import mod.Reference;
import mod.Jeringuillas.JeringuillaBuena;
import mod.Jeringuillas.JeringuillaVacia;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;

public class PlacaCorrectaF extends PlacaPetriAbstract
{
	public static PlacaCorrectaF my_block;
	
	public PlacaCorrectaF()
	{
		super();
	}
	
	public static void init()
	{
		my_block = (PlacaCorrectaF) new PlacaCorrectaF().setUnlocalizedName("placa_correcta_final");
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
		/*
        if(playerIn.getHeldItem() != null)
        {
        	Item held = playerIn.getHeldItem().getItem();
        	
        	if(held instanceof JeringuillaVacia)
        	{
        		playerIn.inventory.setCurrentItem(JeringuillaBuena.my_item, 1, true, false);
        	}
        	
        	return true;
        }
        */
        return false;
    }
}
