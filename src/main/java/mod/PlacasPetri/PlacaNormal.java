package mod.PlacasPetri;

import mod.Reference;
import mod.init.CrazyBlocks.CrazyBlock;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Elementos.Element_Composite_Repressilator_Final;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class PlacaNormal extends PlacaPetriAbstract
{
	public static PlacaNormal my_block;
	
	public PlacaNormal()
	{
		super();
	}
	
	public static void init()
	{
		my_block = (PlacaNormal) new PlacaNormal().setUnlocalizedName("placa_petri");
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}
	
	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		Item held = playerIn.getHeldItem().getItem();
		
		if(held != null)
		{
			Block blockHeld = Block.getBlockFromItem(held);
			if(blockHeld != null && blockHeld instanceof BiologicalElement)
			{
				playerIn.inventory.consumeInventoryItem(held);

				if(blockHeld instanceof PoweredElement)
				{
					if(((PoweredElement) blockHeld).isFinal())
					{
						mod.Util.sendChatMessageAsMod("Elemento powered es final!");
						mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
						worldIn.setBlockState(pos, PlacaCorrectaF.my_block.getDefaultState());
					}
					else
					{
						mod.Util.sendChatMessageAsMod("Elemento powered no es final!");
						mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_ANGRY);
						worldIn.setBlockState(pos, PlacaCorrectaNF.my_block.getDefaultState());
					}
				}
				else if(blockHeld instanceof Element_Composite_Repressilator_Final)
				{
					mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
					worldIn.setBlockState(pos, CrazyBlock.my_block.getDefaultState());
					(worldIn.getBlockState(pos).getBlock()).onBlockPlaced(worldIn, pos, EnumFacing.DOWN, 1F, 1F, 1F, 1, null);
				}
				else
				{
					mod.Util.sendChatMessageAsMod("Elemento no es powered!");
					mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_ANGRY);
					worldIn.setBlockState(pos, PlacaIncorrecta.my_block.getDefaultState());
				}
			}
			
		}

		return true;
	}
}
