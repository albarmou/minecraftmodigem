package mod.Enfermedades;

import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.registry.LanguageRegistry;

public class ColeraPotion extends Potion
{
	//public static final Potion hunger = (new Potion(17, new ResourceLocation("hunger"), true, 5797459)).setPotionName("potion.hunger").setIconIndex(1, 1);
	public static ColeraPotion my_potion;
	
	public ColeraPotion()
	{
		super(32, new ResourceLocation("wither"), true, 5797459);
	}
	
	public static void init()
	{
		my_potion = (ColeraPotion) new ColeraPotion().setPotionName("potion.colera");
		my_potion.setIconIndex(1, 1);//mirar en el jar inventory.png para los iconos, (3,1) vomita, (4,0) espada con sangre, (2,1) conejito
	}
}
