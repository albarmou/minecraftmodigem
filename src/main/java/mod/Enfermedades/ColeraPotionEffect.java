package mod.Enfermedades;

import java.util.ArrayList;

import mod.curas.CuraColera;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;

public class ColeraPotionEffect extends PotionEffect
{
	public ColeraPotionEffect()
	{
		super(ColeraPotion.my_potion.id, Integer.MAX_VALUE, 3, false, true);
		ArrayList<ItemStack> cur = new ArrayList<ItemStack>();
		cur.add(new ItemStack(CuraColera.my_item));
		this.setCurativeItems(cur);
	}
}
