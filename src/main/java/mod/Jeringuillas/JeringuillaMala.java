package mod.Jeringuillas;

import mod.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class JeringuillaMala extends Item
{
	public static JeringuillaMala my_item;
	
	public JeringuillaMala()
	{
		super();
		setMaxStackSize(1);
	}
	
	public static void init()
	{
		my_item = (JeringuillaMala) new JeringuillaMala().setUnlocalizedName("jeringuilla_mala");
	}
	
	public static void register()
	{
		GameRegistry.registerItem(my_item, my_item.getUnlocalizedName().substring(5));
	}
	
	public static void registerRender()
	{
		registerRender(my_item);
	}
	
	public static void registerRender(Item item)
	{
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}

}
