package mod.Jeringuillas;

import mod.Reference;
import mod.PlacasPetri.PlacaCorrectaF;
import mod.PlacasPetri.PlacaPetriAbstract;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class JeringuillaVacia extends Item
{
	public static JeringuillaVacia my_item;
	
	public JeringuillaVacia()
	{
		super();
		setMaxStackSize(1);//o no?
	}
	
	public static void init()
	{
		my_item = (JeringuillaVacia) new JeringuillaVacia().setUnlocalizedName("jeringuilla");
	}
	
	public static void register()
	{
		GameRegistry.registerItem(my_item, my_item.getUnlocalizedName().substring(5));
	}
	
	public static void registerRender()
	{
		registerRender(my_item);
	}
	
	public static void registerRender(Item item)
	{
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
	
	/**
     * Called when a Block is right-clicked with this Item
     *  
     * @param pos The block being right-clicked
     * @param side The side being right-clicked
     *
     */
	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        Block clickedBlock = null;
        if(worldIn.getBlockState(pos) != null)
        {
        	clickedBlock = worldIn.getBlockState(pos).getBlock();
        	if(clickedBlock != null)
        	{	        	
	        	if(clickedBlock instanceof PlacaPetriAbstract)
	        	{
	        		if(clickedBlock instanceof PlacaCorrectaF)
	        		{
	        			mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_HAPPY);
	        			//es bueno, so spanish many wow
	        			playerIn.getHeldItem().setItem(JeringuillaBuena.my_item);
	        		}
	        		else
	        		{
	        			mod.Util.spawParticlesAroundBlock(worldIn, pos, EnumParticleTypes.VILLAGER_ANGRY);
	        			//es malo
	        			playerIn.getHeldItem().setItem(JeringuillaMala.my_item);
	        		}
	        		
	        		return true;
	        	}
        	}
        	
        }
        
        return false;
    }
	/**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
	@Override
    public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn)
    {
		onItemUse(itemStackIn, playerIn, worldIn, playerIn.rayTrace(5, 1.0F).getBlockPos(), EnumFacing.UP, 0.0F, 0.0F, 0.0F);
        return itemStackIn;
    }
}
