package mod.Jeringuillas;

import mod.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class JeringuillaBuena extends Item
{
	public static JeringuillaBuena my_item;
	
	public JeringuillaBuena()
	{
		super();
		setMaxStackSize(1);
	}
	
	public static void init()
	{
		my_item = (JeringuillaBuena) new JeringuillaBuena().setUnlocalizedName("jeringuilla_buena");
	}
	
	public static void register()
	{
		GameRegistry.registerItem(my_item, my_item.getUnlocalizedName().substring(5));
	}
	
	public static void registerRender()
	{
		registerRender(my_item);
	}
	
	public static void registerRender(Item item)
	{
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
