package AlexIngenieriaBiologica.Tipos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import AlexIngenieriaBiologica.Reference.ElementType;

/**
 * Interface class for Biological element.
 * @author Alex Barberá Mourelle
 *
 */
public class BiologicalElement extends Block
{	
	protected String name;
	protected ElementType type;
	protected String description = "";
	protected boolean finalElement;
	protected boolean powered = false;
	
	protected PoweredElement poweredElementPointer = null;
	
	private boolean touched = false;
	
	public BiologicalElement(Material material)
	{
		super(material);
	}
	
	public BiologicalElement(ElementType tipo, String name, String cadena, boolean isFinal, PoweredElement poweredPointer) 
	{
		super(Material.glass);
		this.name = name;
		this.type = tipo;
		this.finalElement = isFinal;
		//this.description = cadena;
		GetCadenaFromFile();
		this.poweredElementPointer = poweredPointer;
		setHardness(2F);
		setResistance(2F);
	}
	
	public BiologicalElement(ElementType tipo, String name, String cadena, boolean isFinal, boolean isPowered)
	{
		super(Material.glass);
		this.name = name;
		this.type = tipo;
		this.finalElement = isFinal;
		//this.description = cadena;
		GetCadenaFromFile();
		this.powered = isPowered;
		setHardness(2F);
		setResistance(2F);
	}

	private void GetCadenaFromFile()
	{
		if(this.description == "")
		{
			//si ponemos el tag de SideOnly(Side.client) cuando lo metemos en el servidor peta asi una vez ya lo tenga si se llama de nuevo que no haga nada
			String fileName = this.name;
			fileName = fileName.contains("_") ? fileName.toUpperCase() : fileName;
			String read = "";
			String path = "assets/" + Reference.MOD_ID + "/Fastas/" + /*fastaFile1.txt";//*/ fileName + ".txt";
			
			System.out.println("Starting fasta file for ... " + this.name);
			
			try 
			{
				InputStream is = this.getClass().getClassLoader().getResourceAsStream(path);//esto es null en el servidor? why m8?

				if(is == null)
				{
					System.err.println("Input stream is null");
					return;
				}
				
				InputStreamReader isr = new InputStreamReader(is);
				
				if(isr == null)
				{
					System.err.println("Input stream reader is null");
					return;
				}
				
				BufferedReader br = new BufferedReader(isr);
			
				System.out.println("reading");
				while((read = br.readLine()) != null)
				{
					this.description += read + "\n";
				}
				br.close();
				
				System.out.println("Done fasta file for: " + this.name);
			} 
			catch (IOException e)
			{
				System.err.println("Not found fasta for: " + this.name);
				this.description = "";
			}
			catch(Exception e)
			{
				System.err.println(e.getStackTrace()[1].getLineNumber() + ": " + e.getLocalizedMessage() + "  in reading fasta for: " + this.name);
				this.description = "";
			}
		}
	}

	public ElementType GetTipe()
	{
		return type;		
	}
	
	public String GetName() 
	{
		return name;
	}
	
	public String GetDescription()
	{
		return description;
	}
	
	public boolean isFinal() 
	{
		return finalElement;
	}
	
	public PoweredElement getPoweredVersion()
	{
		return this.poweredElementPointer;
	}
	
	@Deprecated
	public void powerElement(World worldIn,  BlockPos pos)
	{
		if(!this.powered)
		{
			worldIn.destroyBlock(pos, false);
			worldIn.setBlockState(pos, this.poweredElementPointer.getDefaultState());
		}
		else
		{
			//this no deberia pasar
			//ya esta powered lo matamos
			worldIn.destroyBlock(pos, false);
			mod.Util.sendChatMessageAsMod("Ya estaba cargado y los has matado!!!");
		}
	}
	
	public boolean hasBeenPowered()
	{
		return this.powered;
	}
	
	/**
     * Called when a neighboring block changes.
     */
	@Deprecated
    public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock) 
    {
    	//pos es la posicion de este bloque, esta implementación hace tal cual lo que se necesita
    	if(worldIn.isBlockPowered(pos))
    	{
    		double delta = 1.2;
    		worldIn.spawnParticle(EnumParticleTypes.VILLAGER_HAPPY, (double)pos.getX() + delta, (double)pos.getY() + delta, (double)pos.getZ() + delta, delta, delta, delta, 5);
    		/*
    		EnumChatFormatting f = EnumChatFormatting.AQUA;
    		String m = f + "Is not powered, powering...";
    		ChatComponentText c = new ChatComponentText(m);
    		c.setChatStyle(new ChatStyle().setColor(f));
    		*/
    		if(this.hasBeenPowered())
    		{
    			//Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentText("Is powered."));
    			mod.Util.sendChatMessageAsMod("Is powered.");
    		}
    		else
    		{
    			//Minecraft.getMinecraft().thePlayer.addChatComponentMessage(c);
    			mod.Util.sendChatMessageAsMod("Is not powered, powering...");
    		}
    		
    		this.powerElement(worldIn, pos);
    	}
    }
	
	@Override
	public String toString()
	{
		String cuttedDescription = this.description;
		
		cuttedDescription = cuttedDescription.length() > 25 ? cuttedDescription.subSequence(0, cuttedDescription.indexOf("\n") + 25) + "..." : cuttedDescription;
		
		if(cuttedDescription.length() == 0)
		{
			cuttedDescription = "FASTA -> ERROR";
		}
		
		return this.name + "(" + type.toString() + ", Final element:" + finalElement + ", Powered: " + this.powered + ")\n" + cuttedDescription;
	}
	
	//Cuando boton izquierdo
	@Override
    public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn) 
	{
		this.touched = !this.touched;
		
		if(this.touched)
			mod.Util.sendChatMessageAsMod(this.toString());
	}
}
