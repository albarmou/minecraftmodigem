package AlexIngenieriaBiologica.Tipos;

import AlexIngenieriaBiologica.Reference.ElementType;

public class PoweredElement extends BiologicalElement
{
	public PoweredElement(ElementType tipo, String name, String cadena, boolean isFinal)
	{
		super(tipo, name, cadena, isFinal, true);
	}
}
