package AlexIngenieriaBiologica.Tipos;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Reference.ElementType;

public class ADN_Malo extends BiologicalElement
{
	public static ADN_Malo my_block;
	public static PoweredElement poweredPointer = new PoweredElement(ElementType.OTHER, "adn_negro_powered", "", false);
	
	public ADN_Malo() 
	{
		super(ElementType.OTHER, "adn_negro", "", false, poweredPointer);
	}
	
	@Override
	public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock) 
    {
		//na de na
    }
	
	public static void init()
	{
		my_block = (ADN_Malo) new ADN_Malo().setUnlocalizedName("adn_negro");
	}
	
	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
	}
	
	public static void registerRenders()
	{
		registerRenders(my_block);
	}

	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
