package AlexIngenieriaBiologica;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;


public class Reference 
{
	private static ArrayList<String> names;
	
	public enum ElementType
	{
		PROMOTER, CDS, TERMINATOR, OTHER, FINAL, TU;
		
		public static ElementType parse(String input)
		{
			String a = input.toUpperCase();
			
			if(a == "PROM")
					return PROMOTER;
			else if(a == "CDS")
					return CDS;
			else if( a == "TERMINADOR")//hasta la vista lol
					return TERMINATOR;
			else if (a == "OTHER")
					return OTHER;
			else if(a == "FINAL")
					return FINAL;
			else if (a == "TU")
					return TU;
			else
					return OTHER;
		}
	}
	
	public static boolean isBiological(String unlocalizedName)
	{
		boolean output = false;
		
		if(names == null)
		{
			try
			{
				names = new ArrayList<String>();
				Scanner s = new Scanner(new File("Resoures/names.txt"));
				String a = "";
				while(s.hasNext())
				{
					a = s.nextLine();
					a = a.substring(a.indexOf("_"), a.indexOf("."));
					System.out.println(a);
					output = output || (a == (unlocalizedName));
					if(!names.contains(a))
					{
						names.add(a);
					}
					
				}
				
				s.close();
			}
			catch(Exception e)
			{
				
			}
			
			return output;
		}
		else
		{
			return names.contains(unlocalizedName);
		}
	}
}
