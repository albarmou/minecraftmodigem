package AlexIngenieriaBiologica.Elementos;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Reference.ElementType;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class Element_Pnos extends BiologicalElement
{
	public static Element_Pnos my_block;

	public static PoweredElement poweredPointer = (PoweredElement) new PoweredElement(ElementType.OTHER, "Pnos", "", true).setUnlocalizedName("Pnos_(Powered)");
	
	public Element_Pnos()
	{
		super(ElementType.PROMOTER, "Pnos", "", false, false);
	}
	
	public static void init()
	{
		my_block = (Element_Pnos) new Element_Pnos().setUnlocalizedName("Pnos");
	}

	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(poweredPointer, poweredPointer.getUnlocalizedName().substring(5));
	}

	public static void registerRenders()
	{
		registerRenders(my_block);
		registerRenders(poweredPointer);
	}

	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
