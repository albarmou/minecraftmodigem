package AlexIngenieriaBiologica.Elementos;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Reference.ElementType;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class Element_Pnos_GFP_Tnos extends BiologicalElement
{
	public static Element_Pnos_GFP_Tnos my_block;

	public static PoweredElement poweredPointer = (PoweredElement) new PoweredElement(ElementType.OTHER, "Pnos_GFP_Tnos", "", true).setUnlocalizedName("Pnos_GFP_Tnos_(Powered)");
	
	public Element_Pnos_GFP_Tnos()
	{
		super(ElementType.OTHER, "Pnos_GFP_Tnos", "", true, false);
	}

	public static void init()
	{
		my_block = (Element_Pnos_GFP_Tnos) new Element_Pnos_GFP_Tnos().setUnlocalizedName("Pnos_GFP_Tnos");
	}

	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(poweredPointer, poweredPointer.getUnlocalizedName().substring(5));
	}

	public static void registerRenders()
	{
		registerRenders(my_block);
		registerRenders(poweredPointer);
	}

	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}
