package AlexIngenieriaBiologica.Elementos;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameData;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Reference.ElementType;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class Element_Stafer_fragment extends BiologicalElement
{
	public static Element_Stafer_fragment my_block;
	public static final String name = "Stafer_fragment";
	public static final int id = 4;
	public static final String description = "";
	public static PoweredElement poweredPointer = (PoweredElement) new PoweredElement(ElementType.TU, name, description, false).setUnlocalizedName("Stafer_fragment_(Powered)");

	public Element_Stafer_fragment()
	{
		super(ElementType.TU, name, description, false, poweredPointer);
	}

	public static void init()
	{
		my_block = (Element_Stafer_fragment) new Element_Stafer_fragment().setUnlocalizedName("Stafer_fragment");
	}

	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(poweredPointer, poweredPointer.getUnlocalizedName().substring(5));
	}

	public static void registerRenders()
	{
		registerRenders(my_block);
		registerRenders(poweredPointer);
	}

	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}