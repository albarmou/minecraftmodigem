package AlexIngenieriaBiologica.Elementos;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameData;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Reference.ElementType;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class Element_Product_Toggle_Blue extends BiologicalElement
{
	public static Element_Product_Toggle_Blue my_block;
	public static final String name = "Product_Toggle_Blue";
	public static final int id = 21;
	public static final String description = "";
	public static PoweredElement poweredPointer = (PoweredElement) new PoweredElement(ElementType.OTHER, name, description, false).setUnlocalizedName("Product_Toggle_Blue_(Powered)");

	public Element_Product_Toggle_Blue()
	{
		super(ElementType.OTHER, name, description, false, poweredPointer);
	}

	public static void init()
	{
		my_block = (Element_Product_Toggle_Blue) new Element_Product_Toggle_Blue().setUnlocalizedName("Product_Toggle_Blue");
	}

	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(poweredPointer, poweredPointer.getUnlocalizedName().substring(5));
	}

	public static void registerRenders()
	{
		registerRenders(my_block);
		registerRenders(poweredPointer);
	}

	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}