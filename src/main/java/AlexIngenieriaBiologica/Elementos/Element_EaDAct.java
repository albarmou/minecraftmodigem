package AlexIngenieriaBiologica.Elementos;

import mod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameData;
import net.minecraftforge.fml.common.registry.GameRegistry;
import AlexIngenieriaBiologica.Tipos.BiologicalElement;
import AlexIngenieriaBiologica.Reference.ElementType;
import AlexIngenieriaBiologica.Tipos.PoweredElement;

public class Element_EaDAct extends BiologicalElement
{
	public static Element_EaDAct my_block;
	public static final String name = "EaDAct";
	public static final int id = 8;
	public static final String description = "";
	public static PoweredElement poweredPointer = (PoweredElement) new PoweredElement(ElementType.CDS, name, description, false).setUnlocalizedName("EaDAct_(Powered)");

	public Element_EaDAct()
	{
		super(ElementType.CDS, name, description, false, poweredPointer);
	}

	public static void init()
	{
		my_block = (Element_EaDAct) new Element_EaDAct().setUnlocalizedName("EaDAct");
	}

	public static void register()
	{
		GameRegistry.registerBlock(my_block, my_block.getUnlocalizedName().substring(5));
		GameRegistry.registerBlock(poweredPointer, poweredPointer.getUnlocalizedName().substring(5));
	}

	public static void registerRenders()
	{
		registerRenders(my_block);
		registerRenders(poweredPointer);
	}

	public static void registerRenders(Block block)
	{
		Item item = Item.getItemFromBlock(block);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
	}
}