---- Minecraft Crash Report ----
// Surprise! Haha. Well, this is awkward.

Time: 27/08/15 15:00
Description: Unexpected error

java.lang.StringIndexOutOfBoundsException: String index out of range: 24
	at java.lang.String.substring(Unknown Source)
	at java.lang.String.subSequence(Unknown Source)
	at AlexIngenieriaBiologica.Tipos.BiologicalElement.toString(BiologicalElement.java:189)
	at AlexIngenieriaBiologica.Tipos.BiologicalElement.onBlockClicked(BiologicalElement.java:201)
	at net.minecraft.client.multiplayer.PlayerControllerMP.func_180511_b(PlayerControllerMP.java:247)
	at net.minecraft.client.Minecraft.clickMouse(Minecraft.java:1519)
	at net.minecraft.client.Minecraft.runTick(Minecraft.java:2126)
	at net.minecraft.client.Minecraft.runGameLoop(Minecraft.java:1087)
	at net.minecraft.client.Minecraft.run(Minecraft.java:376)
	at net.minecraft.client.main.Main.main(Main.java:117)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Stacktrace:
	at java.lang.String.substring(Unknown Source)
	at java.lang.String.subSequence(Unknown Source)
	at AlexIngenieriaBiologica.Tipos.BiologicalElement.toString(BiologicalElement.java:189)
	at AlexIngenieriaBiologica.Tipos.BiologicalElement.onBlockClicked(BiologicalElement.java:201)
	at net.minecraft.client.multiplayer.PlayerControllerMP.func_180511_b(PlayerControllerMP.java:247)
	at net.minecraft.client.Minecraft.clickMouse(Minecraft.java:1519)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [EntityPlayerSP['Player939'/142, l='MpServer', x=-215,36, y=64,00, z=-208,23]]
	Chunk stats: MultiplayerChunkCache: 289, 289
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: -232,00,64,00,-184,00 - World: (-232,64,-184), Chunk: (at 8,4,8 in -15,-12; contains blocks -240,0,-192 to -225,255,-177), Region: (-1,-1; contains chunks -32,-32 to -1,-1, blocks -512,0,-512 to -1,255,-1)
	Level time: 234538 game time, 11593 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: survival (ID 0). Hardcore: false. Cheats: false
	Forced entities: 75 total; [EntityCreeper['Creeper'/5888, l='MpServer', x=-152,50, y=33,00, z=-234,50], EntityCreeper['Creeper'/5768, l='MpServer', x=-251,50, y=28,00, z=-265,50], EntityCreeper['Creeper'/5769, l='MpServer', x=-246,91, y=28,00, z=-268,72], EntityCreeper['Creeper'/5770, l='MpServer', x=-249,50, y=28,00, z=-267,50], EntityCreeper['Creeper'/6415, l='MpServer', x=-150,06, y=33,00, z=-237,69], EntityCreeper['Creeper'/6416, l='MpServer', x=-147,00, y=33,00, z=-243,44], EntityCreeper['Creeper'/7824, l='MpServer', x=-236,50, y=38,01, z=-249,50], EntityBat['Bat'/4756, l='MpServer', x=-287,80, y=19,72, z=-247,67], EntityZombie['Zombie'/21, l='MpServer', x=-279,50, y=22,00, z=-256,50], EntityCreeper['Creeper'/5527, l='MpServer', x=-289,38, y=22,00, z=-264,09], EntityZombie['Zombie'/153, l='MpServer', x=-272,50, y=35,00, z=-217,50], EntityCreeper['Creeper'/26, l='MpServer', x=-272,66, y=21,00, z=-237,09], EntityPlayerSP['Player939'/142, l='MpServer', x=-215,36, y=64,00, z=-208,23], EntityBat['Bat'/27, l='MpServer', x=-254,53, y=14,10, z=-220,75], EntitySkeleton['Skeleton'/3868, l='MpServer', x=-273,84, y=26,00, z=-234,47], EntityBat['Bat'/29, l='MpServer', x=-247,96, y=12,22, z=-212,22], EntitySkeleton['Skeleton'/3869, l='MpServer', x=-280,50, y=26,00, z=-232,50], EntityZombie['Zombie'/31, l='MpServer', x=-282,13, y=23,00, z=-214,72], EntityZombie['Zombie'/32, l='MpServer', x=-277,72, y=23,00, z=-209,56], EntitySheep['Sheep'/34, l='MpServer', x=-272,50, y=62,42, z=-193,03], EntityBat['Bat'/37, l='MpServer', x=-266,66, y=31,05, z=-284,67], EntityZombie['Zombie'/38, l='MpServer', x=-271,50, y=26,00, z=-241,50], EntityCreeper['Creeper'/39, l='MpServer', x=-259,50, y=28,00, z=-240,50], EntitySkeleton['Skeleton'/5031, l='MpServer', x=-147,50, y=24,00, z=-270,50], EntityCreeper['Creeper'/41, l='MpServer', x=-259,50, y=28,00, z=-238,50], EntityCreeper['Creeper'/42, l='MpServer', x=-260,50, y=28,00, z=-236,50], EntityCreeper['Creeper'/170, l='MpServer', x=-259,50, y=44,00, z=-170,50], EntitySkeleton['Skeleton'/171, l='MpServer', x=-235,50, y=32,00, z=-265,50], EntityCreeper['Creeper'/4011, l='MpServer', x=-227,66, y=33,00, z=-253,94], EntityCreeper['Creeper'/45, l='MpServer', x=-262,50, y=26,00, z=-216,50], EntityChicken['Chicken'/46, l='MpServer', x=-262,03, y=62,71, z=-217,19], EntityZombie['Zombie'/174, l='MpServer', x=-254,50, y=32,00, z=-191,50], EntitySheep['Sheep'/47, l='MpServer', x=-268,38, y=63,00, z=-200,09], EntityZombie['Zombie'/175, l='MpServer', x=-253,50, y=32,00, z=-191,50], EntityChicken['Chicken'/48, l='MpServer', x=-264,28, y=62,63, z=-205,56], EntityZombie['Zombie'/5040, l='MpServer', x=-292,50, y=23,00, z=-261,50], EntityItem['item.item.egg'/49, l='MpServer', x=-263,50, y=62,00, z=-205,13], EntitySkeleton['Skeleton'/5041, l='MpServer', x=-293,50, y=23,00, z=-261,50], EntityCow['Cow'/50, l='MpServer', x=-258,22, y=67,00, z=-194,19], EntitySkeleton['Skeleton'/5042, l='MpServer', x=-288,50, y=24,00, z=-267,16], EntityCow['Cow'/51, l='MpServer', x=-265,91, y=62,40, z=-186,84], EntitySkeleton['Skeleton'/5043, l='MpServer', x=-291,25, y=23,00, z=-266,28], EntityCow['Cow'/52, l='MpServer', x=-270,09, y=62,38, z=-187,28], EntityCow['Cow'/53, l='MpServer', x=-258,06, y=65,01, z=-184,28], EntitySheep['Sheep'/54, l='MpServer', x=-259,56, y=65,01, z=-185,19], EntityPig['Pig'/55, l='MpServer', x=-259,53, y=66,00, z=-189,88], EntityBat['Bat'/56, l='MpServer', x=-261,34, y=43,10, z=-165,38], EntitySkeleton['Skeleton'/62, l='MpServer', x=-255,50, y=21,00, z=-212,50], EntityChicken['Chicken'/63, l='MpServer', x=-242,34, y=62,53, z=-217,53], EntityCow['Cow'/64, l='MpServer', x=-255,03, y=68,00, z=-197,88], EntityCreeper['Creeper'/193, l='MpServer', x=-158,50, y=21,00, z=-240,50], EntityBat['Bat'/3521, l='MpServer', x=-255,50, y=36,97, z=-226,55], EntityBat['Bat'/3522, l='MpServer', x=-262,21, y=35,04, z=-211,77], EntityItem['item.tile.35s_(Powered)'/67, l='MpServer', x=-224,66, y=66,00, z=-190,28], EntityCreeper['Creeper'/8134, l='MpServer', x=-185,50, y=24,02, z=-205,50], EntitySpider['Spider'/72, l='MpServer', x=-218,31, y=29,00, z=-235,31], EntitySkeleton['Skeleton'/75, l='MpServer', x=-206,50, y=13,00, z=-143,50], EntityBat['Bat'/6093, l='MpServer', x=-155,28, y=22,04, z=-232,44], EntityZombie['Zombie'/8269, l='MpServer', x=-158,50, y=13,02, z=-188,50], EntityWitch['Witch'/4178, l='MpServer', x=-295,38, y=11,00, z=-176,91], EntityCreeper['Creeper'/83, l='MpServer', x=-179,38, y=33,00, z=-217,91], EntityWitch['Witch'/4179, l='MpServer', x=-293,50, y=11,00, z=-180,50], EntityBat['Bat'/6099, l='MpServer', x=-272,48, y=25,61, z=-224,51], EntitySlime['Slime'/5588, l='MpServer', x=-267,50, y=23,02, z=-254,50], EntityBat['Bat'/6100, l='MpServer', x=-266,23, y=25,78, z=-221,60], EntityCreeper['Creeper'/5591, l='MpServer', x=-261,50, y=22,00, z=-250,50], EntityBat['Bat'/88, l='MpServer', x=-153,75, y=18,10, z=-210,25], EntityZombie['Zombie'/89, l='MpServer', x=-147,50, y=28,00, z=-177,50], EntityCreeper['Creeper'/3931, l='MpServer', x=-172,50, y=33,00, z=-214,50], EntitySkeleton['Skeleton'/92, l='MpServer', x=-144,25, y=28,00, z=-175,50], EntityZombie['Zombie'/93, l='MpServer', x=-139,91, y=27,00, z=-175,25], EntitySkeleton['Skeleton'/96, l='MpServer', x=-142,50, y=20,00, z=-208,50], EntitySpider['Spider'/3814, l='MpServer', x=-274,50, y=20,00, z=-236,50], EntityZombie['Zombie'/5607, l='MpServer', x=-201,50, y=26,00, z=-133,50], EntityCreeper['Creeper'/6253, l='MpServer', x=-144,56, y=24,00, z=-258,94]]
	Retry entities: 0 total; []
	Server brand: fml,forge
	Server type: Integrated singleplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.addWorldInfoToCrashReport(WorldClient.java:392)
	at net.minecraft.client.Minecraft.addGraphicsAndWorldToCrashReport(Minecraft.java:2614)
	at net.minecraft.client.Minecraft.run(Minecraft.java:405)
	at net.minecraft.client.main.Main.main(Main.java:117)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at net.minecraftforge.gradle.GradleStartCommon.launch(Unknown Source)
	at GradleStart.main(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.8
	Operating System: Windows 7 (x86) version 6.1
	Java Version: 1.8.0_45, Oracle Corporation
	Java VM Version: Java HotSpot(TM) Client VM (mixed mode), Oracle Corporation
	Memory: 811446560 bytes (773 MB) / 1060372480 bytes (1011 MB) up to 1060372480 bytes (1011 MB)
	JVM Flags: 3 total; -Xincgc -Xmx1024M -Xms1024M
	IntCache: cache: 0, tcache: 0, allocated: 12, tallocated: 94
	FML: MCP v9.10 FML v8.0.99.99 Minecraft Forge 11.14.3.1487 4 mods loaded, 4 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
	UCHIJAAAA	mcp{9.05} [Minecraft Coder Pack] (minecraft.jar) 
	UCHIJAAAA	FML{8.0.99.99} [Forge Mod Loader] (forgeSrc-1.8-11.14.3.1487.jar) 
	UCHIJAAAA	Forge{11.14.3.1487} [Minecraft Forge] (forgeSrc-1.8-11.14.3.1487.jar) 
	UCHIJAAAA	mod_id{1.0} [Mod Ingenieria biologica] (bin) 
	Loaded coremods (and transformers): 
	GL info: ' Vendor: 'NVIDIA Corporation' Version: '4.4.0' Renderer: 'GeForce GT 630/PCIe/SSE2'
	Launched Version: 1.8
	LWJGL: 2.9.1
	OpenGL: GeForce GT 630/PCIe/SSE2 GL version 4.4.0, NVIDIA Corporation
	GL Caps: Using GL 1.3 multitexturing.
Using GL 1.3 texture combiners.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Shaders are available because OpenGL 2.1 is supported.
VBOs are available because OpenGL 1.5 is supported.

	Using VBOs: No
	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: []
	Current Language: English (US)
	Profiler Position: N/A (disabled)