import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;


public class Main 
{
	private static void doFile(File file)
	{
	    List<String> lines = new ArrayList<String>();
		String line = "";
		
		if(file.getName().endsWith(".json"))
		{
			try
			{
				FileReader fr = new FileReader(file);
	            BufferedReader br = new BufferedReader(fr);
	            while ((line = br.readLine()) != null) 
	            {
	                if (line.contains("mod_id"))
	                    line = line.replace("mod_id", "aladDNA");
	                lines.add(line);
	            }
	            fr.close();
	            br.close();
	
	            FileWriter fw = new FileWriter(file);
	            BufferedWriter out = new BufferedWriter(fw);
	            for(String s : lines)
	                 out.write(s);
	            out.flush();
	            out.close();
	            System.out.println("Done " + file.getPath());
	        } 
			catch (Exception ex) 
			{
	            ex.printStackTrace();
	        }
		}
	}
	
	private static void doSubFolders(File file)
	{
		for(File sub : file.listFiles())
		{
			if(sub.isDirectory())
			{
				doSubFolders(sub);
			}
			else
			{
				doFile(sub);
			}
		}
	}

	public static void main(String[] args) 
	{
		try
		{
			File main = new File("C:/Users/Alex/Desktop/pene/src/main/resources/assets/mod_id");
			
			for(File sub : main.listFiles())
			{
				if(sub.isDirectory())
				{
					doSubFolders(sub);
				}
				else
				{
					doFile(sub);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

}
